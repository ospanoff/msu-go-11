package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"testing"
	"time"
)

type Datum struct {
	ID        int    `xml:"id"`
	FirstName string `xml:"first_name"`
	LastName  string `xml:"last_name"`
	Age       int    `xml:"age"`
	About     string `xml:"about"`
	Gender    string `xml:"gender"`
}

type Data struct {
	Data []Datum `xml:"row"`
}

var Dataset = Data{}

func Search(limit int, offset int, query string, orderField string, orderBy int) []User {
	var users = []User{}

	for _, data := range Dataset.Data {
		var name = data.FirstName + " " + data.LastName
		if strings.Contains(name, query) || strings.Contains(data.About, query) {
			users = append(users, User{
				Id:     data.ID,
				Name:   name,
				Age:    data.Age,
				About:  data.About,
				Gender: data.Gender,
			})
		}
	}

	switch orderField {
	case "Id":
		sort.Slice(users, func(i int, j int) bool {
			if orderBy == -1 {
				return users[i].Id > users[j].Id
			}
			return users[i].Id < users[j].Id
		})
	case "Age":
		sort.Slice(users, func(i int, j int) bool {
			if orderBy == -1 {
				return users[i].Age > users[j].Age
			}
			return users[i].Age < users[j].Age
		})
	default:
		sort.Slice(users, func(i int, j int) bool {
			if orderBy == -1 {
				return users[i].Name > users[j].Name
			}
			return users[i].Name < users[j].Name
		})
	}

	var lbound = offset
	var rbound = offset + limit
	var ldata = len(users)
	if lbound < ldata {
		if rbound > ldata {
			return users[lbound:ldata]
		}
		return users[lbound:rbound]

	}
	return []User{}
}

func TestDoSearch(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.URL.Path {
		case "/":
			q := r.URL.Query()
			limit, err := strconv.Atoi(q.Get("limit"))
			if err != nil {
				limit = 1
			}
			offset, err := strconv.Atoi(q.Get("offset"))
			if err != nil {
				offset = 0
			}
			orderBy, err := strconv.Atoi(q.Get("order_by"))
			if err != nil {
				orderBy = 1
			}

			resp, err := json.Marshal(Search(
				limit,
				offset,
				q.Get("query"),
				q.Get("order_field"),
				orderBy))
			if err != nil {
				w.Write([]byte("{}"))
			} else {
				w.Write(resp)
			}
		case "/timeout":
			time.Sleep(time.Second + time.Millisecond)
		case "/notJson":
			http.Error(w, "notJson", 200)
		default:
			http.Error(w, "Wrong path", 404)
		}
	}))
	defer ts.Close()

	datasetFile, err := ioutil.ReadFile("dataset.xml")
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	xml.Unmarshal(datasetFile, &Dataset)

	for i, testCase := range []struct {
		limit      int
		offset     int
		query      string
		orderField string
		orderBy    int
		err        error
		url        string
		result     *SearchResponse
	}{
		// Строка 63 не покрывается, т.к. NewRequest жует любой hostname
		// Non-error cases
		{2, 0, "Boyd", "Id", orderAsc, nil, ts.URL, &SearchResponse{UsersById[:1], false}}, // General test
		{2, 2, "B", "Id", orderAsc, nil, ts.URL, &SearchResponse{UsersById[2:4], true}},    // Offset test
		{4, 0, "B", "Age", orderAsc, nil, ts.URL, &SearchResponse{UsersByAge, true}},       // Age sort test
		{4, 0, "B", "Name", orderAsc, nil, ts.URL, &SearchResponse{UsersByName, true}},     // Name sort test
		{5, 0, "B", "Id", orderDesc, nil, ts.URL, &SearchResponse{UsersByIdRev, false}},    // Descending sort test
		{1, 10, "фыв", "Id", orderDesc, nil, ts.URL, &SearchResponse{[]User{}, false}},     // Empty response

		// Anser length matter
		{26, 0, "", "", orderAsc, nil, ts.URL, nil}, // max limit test

		// Error cases
		{0, 0, "", "", 1, fmt.Errorf("limit must be > 0"), ts.URL, nil},              // Limit == 0
		{-1, 0, "", "", 1, fmt.Errorf("limit must be > 0"), ts.URL, nil},             // wrong limit test
		{1, -1, "", "", 1, fmt.Errorf("offset must be >= 0"), ts.URL, nil},           // wrong offset lest
		{1, 0, "", "", 1, fmt.Errorf("timeout for"), ts.URL + "/timeout", nil},       // server timeout test
		{1, 0, "", "", 1, fmt.Errorf("Get badurl"), "badurl", nil},                   // bad protocol/url test
		{1, 0, "", "", 1, fmt.Errorf("invalid character"), ts.URL + "/notJson", nil}, // sending not Json test
	} {
		// GET ANSWER
		answer, err := doSearch(testCase.url, testCase.limit, testCase.offset, testCase.query, testCase.orderField, testCase.orderBy)

		if testCase.limit > 25 {
			if len(answer.Users) != 25 {
				t.Errorf("Case [%d]: failed, EXPECTED %d users, GOT %d", i+1, 26, len(answer.Users))
			}
		} else {

			if err != nil {
				if !strings.Contains(err.Error(), testCase.err.Error()) {
					t.Errorf("Case [%d]: failed, \n\tEXPECTED \n%v err, \n\tGOT \n%v err", i+1, testCase.err, err)
				}
			} else {
				if !reflect.DeepEqual(testCase.result, answer) {
					t.Errorf("Case [%d]: failed, \n\tEXPECTED \n%v, \n\tGOT \n%v", i+1, testCase.result, answer)
				}
			}
		}
	}
}

var UsersById = []User{
	User{
		Id:     0,
		Name:   "Boyd Wolf",
		Age:    22,
		About:  "Nulla cillum enim voluptate consequat laborum esse excepteur occaecat commodo nostrud excepteur ut cupidatat. Occaecat minim incididunt ut proident ad sint nostrud ad laborum sint pariatur. Ut nulla commodo dolore officia. Consequat anim eiusmod amet commodo eiusmod deserunt culpa. Ea sit dolore nostrud cillum proident nisi mollit est Lorem pariatur. Lorem aute officia deserunt dolor nisi aliqua consequat nulla nostrud ipsum irure id deserunt dolore. Minim reprehenderit nulla exercitation labore ipsum.\n",
		Gender: "male",
	},
	User{
		Id:     2,
		Name:   "Brooks Aguilar",
		Age:    25,
		About:  "Velit ullamco est aliqua voluptate nisi do. Voluptate magna anim qui cillum aliqua sint veniam reprehenderit consectetur enim. Laborum dolore ut eiusmod ipsum ad anim est do tempor culpa ad do tempor. Nulla id aliqua dolore dolore adipisicing.\n",
		Gender: "male",
	},
	User{
		Id:     5,
		Name:   "Beulah Stark",
		Age:    30,
		About:  "Enim cillum eu cillum velit labore. In sint esse nulla occaecat voluptate pariatur aliqua aliqua non officia nulla aliqua. Fugiat nostrud irure officia minim cupidatat laborum ad incididunt dolore. Fugiat nostrud eiusmod ex ea nulla commodo. Reprehenderit sint qui anim non ad id adipisicing qui officia Lorem.\n",
		Gender: "female",
	},
	User{
		Id:     19,
		Name:   "Bell Bauer",
		Age:    26,
		About:  "Nulla voluptate nostrud nostrud do ut tempor et quis non aliqua cillum in duis. Sit ipsum sit ut non proident exercitation. Quis consequat laboris deserunt adipisicing eiusmod non cillum magna.\n",
		Gender: "male",
	},
	User{
		Id:     22,
		Name:   "Beth Wynn",
		Age:    31,
		About:  "Proident non nisi dolore id non. Aliquip ex anim cupidatat dolore amet veniam tempor non adipisicing. Aliqua adipisicing eu esse quis reprehenderit est irure cillum duis dolor ex. Laborum do aute commodo amet. Fugiat aute in excepteur ut aliqua sint fugiat do nostrud voluptate duis do deserunt. Elit esse ipsum duis ipsum.\n",
		Gender: "female",
	},
}

var UsersByName = []User{
	UsersById[3],
	UsersById[4],
	UsersById[2],
	UsersById[0],
}

var UsersByAge = []User{
	UsersById[0],
	UsersById[1],
	UsersById[3],
	UsersById[2],
}

var UsersByIdRev = []User{
	UsersById[4],
	UsersById[3],
	UsersById[2],
	UsersById[1],
	UsersById[0],
}
