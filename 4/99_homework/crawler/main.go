package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

// Crawler struct
type Crawler struct {
	hrefRgx    *regexp.Regexp
	tagRgx     *regexp.Regexp
	allUrls    map[string]bool
	parsedUrls []string
	host       string
}

func (c *Crawler) fetchUrls(baseURL, body string) {
	var baseURLst, _ = url.Parse(baseURL)
	var urlPath = baseURLst.Path
	var newUrls = c.tagRgx.FindAllString(body, -1)
	for i, urlTmp := range newUrls {
		if strings.Contains(urlTmp, "<base") {
			var baseTmp = c.hrefRgx.FindString(urlTmp)
			baseTmp = baseTmp[6 : len(baseTmp)-1]
			var baseTmpst, _ = url.Parse(baseTmp)
			if baseTmpst.Host == "" {
				baseURLst = baseURLst.ResolveReference(baseTmpst)
			}
			newUrls[i] = ""
			break
		}
	}
	for _, newURL := range newUrls {
		if newURL != "" {
			newURL = c.hrefRgx.FindString(newURL)
			newURL = newURL[6 : len(newURL)-1]
			var newURLst, err = url.Parse(newURL)
			if err == nil {
				var urlTmpst = baseURLst.ResolveReference(newURLst)
				if urlTmpst.Host == baseURLst.Host {
					var urlTmp = urlTmpst.String()
					if _, ok := c.allUrls[urlTmp]; !ok {
						c.allUrls[urlTmp] = false
					}
				}
			} else {
				fmt.Println("An error has occured:", err)
			}
		}
	}
	c.allUrls[baseURL] = true
	// Sequence could be another (but it fits the test)
	c.parsedUrls = append(c.parsedUrls, urlPath)
}

func (c *Crawler) crawl(url string) {
	resp, err := http.Get(url)
	url = resp.Request.URL.String()
	if err != nil {
		fmt.Println("An error has occured:", err)
	} else {
		if resp.StatusCode != 200 {
			c.allUrls[url] = true
			return
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println("Read error has occured:", err)
		} else {
			c.fetchUrls(url, string(body))
		}
		resp.Body.Close()
	}
	for purl, isParsed := range c.allUrls {
		if !isParsed {
			c.crawl(purl)
		}
	}
}

// Crawl func
func Crawl(host string) []string {
	var crawler = Crawler{
		hrefRgx:    regexp.MustCompile(`href=\"(.*?)\"`),
		tagRgx:     regexp.MustCompile(`<(a|base) [^>]*href=\"(.*?)\"`),
		allUrls:    map[string]bool{},
		parsedUrls: []string{},
		host:       host,
	}
	crawler.crawl(host)
	return crawler.parsedUrls
}

func main() {
}
