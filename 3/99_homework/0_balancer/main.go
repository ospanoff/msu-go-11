package main

import "fmt"

// RoundRobinBalancer struct
type RoundRobinBalancer struct {
	nodesNum    int
	load        []int
	currentNode int
	requestChan chan bool
	nodeChan    chan int
}

// Init sets struct fields and runs balancer
func (b *RoundRobinBalancer) Init(n int) {
	b.nodesNum = n
	b.load = make([]int, n)
	b.requestChan = make(chan bool)
	b.nodeChan = make(chan int)
	go b.run()
}

// GiveStat gives balancer load
func (b *RoundRobinBalancer) GiveStat() []int {
	return b.load
}

// GiveNode allocates node
func (b *RoundRobinBalancer) GiveNode() int {
	b.requestChan <- true
	return <-b.nodeChan
}

func (b *RoundRobinBalancer) allocNode() int {
	b.load[b.currentNode]++
	b.currentNode++
	b.currentNode %= b.nodesNum
	return b.currentNode - 1
}

func (b *RoundRobinBalancer) run() {
	for {
		select {
		// If request has come
		case <-b.requestChan:
			b.nodeChan <- b.allocNode()
		}
	}
}

func main() {
	balancer := new(RoundRobinBalancer)
	balancer.Init(2)
	for i := 0; i < 100; i++ {
		balancer.GiveNode()
	}
	fmt.Println(balancer.GiveStat())

}
