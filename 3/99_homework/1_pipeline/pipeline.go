package pipeline

import "sync"

type job func(in, out chan interface{})

func exec(in chan interface{}, fun job, wg *sync.WaitGroup) chan interface{} {
	out := make(chan interface{})
	wg.Add(1)
	go func() {
		fun(in, out)
		close(out)
		wg.Done()
	}()
	return out
}

func execRecursive(in chan interface{}, funcs []job, wg *sync.WaitGroup) chan interface{} {
	if len(funcs) == 0 {
		return make(chan interface{})
	}
	return execRecursive(exec(in, funcs[0], wg), funcs[1:], wg)
}

// Pipe implements pipeline
func Pipe(funcs ...job) {
	var wg sync.WaitGroup
	execRecursive(make(chan interface{}), funcs, &wg)
	wg.Wait()
}
