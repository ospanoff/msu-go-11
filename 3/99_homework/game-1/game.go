package main

var players []*Player

var gameWorld = createWorld()

func initGame() {
	gameWorld.resetWorld()
}

// NewPlayer creates new player
func NewPlayer(name string) *Player {
	return &Player{name, &gameWorld, "кухня", map[string]Item{}, make(chan string)}
}

// addPlayer adds a player to the game
func addPlayer(player *Player) {
	gameWorld.players = append(gameWorld.players, player)
}

func main() {
}
