package main

func getItem(name string, count int) Item {
	switch name {
	case "рюкзак":
		return Item{wearableItem, "рюкзак", count, []string{}}
	case "ключи":
		return Item{takableItem, "ключи", count, []string{"дверь"}}
	case "конспекты":
		return Item{takableItem, "конспекты", count, []string{}}
	default:
		return Item{}
	}
}

func createRooms() map[string]Room {
	return map[string]Room{
		"кухня": Room{
			name:      "кухня",
			items:     map[string]Item{},
			nextRooms: []string{"коридор"},
			obstacles: []string{""},
			message:   "кухня, ничего интересного",
			objects:   map[string]Object{},
		},
		"коридор": Room{
			name:      "коридор",
			items:     map[string]Item{},
			nextRooms: []string{"кухня", "комната", "улица"},
			obstacles: []string{"", "", "дверь"},
			message:   "ничего интересного",
			objects: map[string]Object{
				"дверь": Object{1, []string{"открыта", "закрыта"}}, // 1 if bloked else 0
			},
		},
		"комната": Room{
			name: "комната",
			items: map[string]Item{
				"рюкзак":    getItem("рюкзак", 1),
				"ключи":     getItem("ключи", 1),
				"конспекты": getItem("конспекты", 1),
			},
			nextRooms: []string{"коридор"},
			obstacles: []string{""},
			message:   "ты в своей комнате",
			objects:   map[string]Object{},
		},
		"улица": Room{
			name:      "улица",
			items:     map[string]Item{},
			nextRooms: []string{"домой"},
			obstacles: []string{""},
			message:   "на улице весна",
			objects:   map[string]Object{},
		},
	}
}

func createWorld() World {
	return World{
		rooms: createRooms(),
	}
}
