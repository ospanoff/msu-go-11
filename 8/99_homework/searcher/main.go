package main

import (
	"bufio"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	_ "net/http/pprof"
)

const logsPath = "./data/"

func main() {
	http.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {

		if req.URL.String() == "/favicon.ico" {
			return
		}

		seenBrowsers := map[string]bool{}
		uniqueBrowsers := 0

		files, _ := ioutil.ReadDir(logsPath)
		for _, f := range files {
			file, err := os.Open(logsPath + f.Name())
			if err != nil {
				panic(err)
			}

			w.Write([]byte(f.Name() + "\nfound users:\n"))

			scanner := bufio.NewScanner(file)

			user := User{}
			for i := 0; scanner.Scan(); i++ {
				if user.UnmarshalJSON(scanner.Bytes()) != nil {
					continue
				}

				isAndroid := false
				isMSIE := false

				for _, browser := range user.Browsers {
					if !isAndroid {
						isAndroid = strings.Contains(browser, "Android")
					}
					if !isMSIE {
						isMSIE = strings.Contains(browser, "MSIE")
					}

					if _, ok := seenBrowsers[browser]; !ok {
						log.Printf("New browser: %s, first seen: %s", browser, user.Name)
						seenBrowsers[browser] = true
						uniqueBrowsers++
					}
				}

				if isAndroid && isMSIE {
					log.Println("Android and MSIE user:", user.Name, user.Email)
					email := strings.Replace(user.Email, "@", " [at] ", 1)
					w.Write([]byte("[" + strconv.Itoa(i) + "] " + user.Name + "<" + email + ">\n"))
				}
			}
			if err = scanner.Err(); err != nil {
				panic(err)
			}

			w.Write([]byte("Total unique browsers" + strconv.Itoa(uniqueBrowsers) + "\n\n"))

			file.Close()
		}

	}))
	err := http.ListenAndServe(":8081", nil)
	if err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
