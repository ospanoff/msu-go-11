#!/bin/bash
# Usage: sh run_benchmark.sh fix_num (before|after)
go build
searcher &>/dev/null &

sleep 0.01

wrk -d 40s http://127.0.0.1:8081/ &

mkdir -p ./benchmark_results/$1

go tool pprof -svg searcher http://127.0.0.1:8081/debug/pprof/profile > ./benchmark_results/$1/cpu_$2.svg

wget -O heap.out http://127.0.0.1:8081/debug/pprof/heap
go tool pprof -svg -inuse_space searcher heap.out > ./benchmark_results/$1/inuse_space_$2.svg
go tool pprof -svg -inuse_objects searcher heap.out > ./benchmark_results/$1/inuse_objects_$2.svg
go tool pprof -svg -alloc_space searcher heap.out > ./benchmark_results/$1/alloc_space_$2.svg

sleep 0.01

pkill searcher
pkill wrk
