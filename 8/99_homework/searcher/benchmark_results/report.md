Оптимизации:

- regexp.MatchString был заменен на strings.Contains
- была создана структура User и был добавлен easyjson
- было убрано создание массива юзеров и цикл по юзерам перенесен в цикл по строкам
- для чтения файла построчно используется bufio
- Sprintf заменен на сумму строк
- regexp.ReplaceAllString заменен на strings.Replace
- два цикла по браузерам объединен в один
- обновление списка seenBrowsers перенесен за if-ы
- seenBrowsers теперь map (log(n) раз выигрыш)
- concatstring ответов теперь выдается по отдельности
- fmt.Fprintf заменен на w.Write
- рефакторинг
