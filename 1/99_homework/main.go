package main

import (
	"sort"
	"strconv"
)

// ReturnInt returns int
func ReturnInt() int {
	var i = 1
	return i
}

// ReturnFloat returns float32
func ReturnFloat() float32 {
	var f float32 = 1.1
	return f
}

// ReturnIntArray returns array of ints of len 3
func ReturnIntArray() [3]int {
	var arr = [...]int{1, 3, 4}
	return arr
}

// ReturnIntSlice returns int slice
func ReturnIntSlice() []int {
	sl := []int{1, 2, 3}
	return sl
}

// IntSliceToString func
// input: slice
// output: string made of slice elements
func IntSliceToString(s []int) string {
	var r = ""
	for _, e := range s {
		r += strconv.Itoa(e)
	}
	return r
}

// MergeSlices merges two slices of different types
// input: sl1 float32 slice, sl2 int32 slice
// output: int slice
func MergeSlices(sl1 []float32, sl2 []int32) []int {
	var sl1Len = len(sl1)
	sl := make([]int, sl1Len+len(sl2))
	for i, e := range sl1 {
		sl[i] = int(e)
	}
	for i, e := range sl2 {
		sl[sl1Len+i] = int(e)
	}

	return sl
}

// GetMapValuesSortedByKey func
func GetMapValuesSortedByKey(m map[int]string) []string {
	keys := make([]int, len(m))
	var i int
	for key := range m {
		keys[i] = key
		i++
	}
	sort.Ints(keys)
	values := make([]string, len(m))
	for i, key := range keys {
		values[i] = m[key]
	}
	return values
}
