package main

func getItem(name string, count int) item {
	switch name {
	case "рюкзак":
		return item{wearableItem, "рюкзак", count, []string{}}
	case "ключи":
		return item{takableItem, "ключи", count, []string{"дверь"}}
	case "конспекты":
		return item{takableItem, "конспекты", count, []string{}}
	default:
		return item{}
	}
}

func createRooms() map[string]room {
	return map[string]room{
		"кухня": room{
			name:      "кухня",
			items:     map[string]item{},
			nextRooms: []string{"коридор"},
			obstacles: []string{""},
			message:   "кухня, ничего интересного",
			objects:   map[string]object{},
		},
		"коридор": room{
			name:      "коридор",
			items:     map[string]item{},
			nextRooms: []string{"кухня", "комната", "улица"},
			obstacles: []string{"", "", "дверь"},
			message:   "ничего интересного",
			objects: map[string]object{
				"дверь": object{1, []string{"открыта", "закрыта"}}, // 1 if bloked else 0
			},
		},
		"комната": room{
			name: "комната",
			items: map[string]item{
				"рюкзак":    getItem("рюкзак", 1),
				"ключи":     getItem("ключи", 1),
				"конспекты": getItem("конспекты", 1),
			},
			nextRooms: []string{"коридор"},
			obstacles: []string{""},
			message:   "ты в своей комнате",
			objects:   map[string]object{},
		},
		"улица": room{
			name:      "улица",
			items:     map[string]item{},
			nextRooms: []string{"домой"},
			obstacles: []string{""},
			message:   "на улице весна",
			objects:   map[string]object{},
		},
	}
}

func createWorld() world {
	return world{
		rooms: createRooms(),
	}
}
