package main

import (
	"fmt"
	"strings"
)

// WORLD class
type world struct {
	rooms map[string]room
}

func (w *world) resetWorld() {
	w.rooms = createRooms()
}

// PLAYER class
type player struct {
	currentWorld    *world
	currentRoomName string
	inventory       map[string]item
}

// ROOM class
type room struct {
	name      string
	items     map[string]item
	nextRooms []string
	obstacles []string
	message   string
	objects   map[string]object
}

// ITEM class: objects that are takable by player, e.g. key
type item struct {
	itemType   string
	name       string
	quantity   int
	applicable []string
}

const (
	wearableItem = "wearable"
	takableItem  = "takable"
)

// OBJECT class: objects that are built in rooms, e.g. door
type object struct {
	currentState int
	states       []string
}

// GAME FUNTIONS
var errorMessage = "неизвестная команда"

func handleCommand(s string) string {
	var args = strings.Split(s, " ")
	var params = args[1:]
	var cmd = args[0]
	action, ok := actions[cmd]
	if !ok {
		return errorMessage
	}
	return action(&players[currentPlayer], params...)
}

func takeItem(Player *player, itemName string, itemType string) (string, bool) {
	if currentRoom, okRoom := Player.currentWorld.rooms[Player.currentRoomName]; okRoom {
		if itemInRoom, okItem := currentRoom.items[itemName]; okItem {
			if itemInRoom.itemType != itemType {
				return errorMessage, true
			}
			if itemInRoom.quantity > 0 {
				itemInRoom.quantity--
				currentRoom.items[itemName] = itemInRoom

				if it, okIt := Player.inventory[itemName]; okIt {
					it.quantity++
					Player.inventory[itemName] = it
				} else {
					itemInRoom.quantity = 1
					Player.inventory[itemName] = itemInRoom
				}
				return itemName, false
			}
		}
		return "нет такого", true
	}
	return errorMessage, true
}

var actions = map[string]func(*player, ...string) string{
	"осмотреться": func(Player *player, args ...string) string {
		if len(args) == 0 {
			// FIXME DELETE THIS WORKAROUND
			if Player.currentRoomName == "кухня" {
				for name := range Player.inventory {
					if name == "рюкзак" {
						return "ты находишься на кухне, на столе чай, надо идти в универ. можно пройти - коридор"
					}
				}
				return "ты находишься на кухне, на столе чай, надо собрать рюкзак и идти в универ. можно пройти - коридор"
			}
			var w, t int
			if currentRoom, ok := Player.currentWorld.rooms[Player.currentRoomName]; ok {
				var wearableItems = "на стуле - "
				var takableItems = "на столе: "
				var allItems = getMapValuesSortedByKey(currentRoom.items)
				for _, item := range allItems {
					if item.quantity > 0 {
						if item.itemType == wearableItem {
							wearableItems += item.name + ", "
							w++
						} else if item.itemType == "takable" {
							takableItems += item.name + ", "
							t++
						}
					}
				}
				if w == 0 {
					wearableItems = ""
					if t != 0 {
						takableItems = takableItems[:len(takableItems)-2]
					}
				} else {
					wearableItems = wearableItems[:len(wearableItems)-2]
				}
				if t == 0 {
					takableItems = ""
				}
				if w+t == 0 {
					takableItems = "пустая комната"
				}
				return fmt.Sprintf("%s%s. можно пройти - %s", takableItems, wearableItems, strings.Join(currentRoom.nextRooms, ", "))
			}
		}
		return errorMessage
	},

	"идти": func(Player *player, args ...string) string {
		if len(args) == 1 {
			if currentRoom, ok := Player.currentWorld.rooms[Player.currentRoomName]; ok {
				for i, r := range currentRoom.nextRooms {
					if r == args[0] {
						var obstacle = currentRoom.obstacles[i]
						if obj, okObj := currentRoom.objects[obstacle]; okObj {
							if obj.currentState == 1 {
								return obstacle + " " + obj.states[obj.currentState]
							}
						}
						if currentRoom, ok = Player.currentWorld.rooms[args[0]]; ok {
							Player.currentRoomName = args[0]
							return fmt.Sprintf("%s. можно пройти - %s", currentRoom.message, strings.Join(currentRoom.nextRooms, ", "))
						}
						return errorMessage
					}
				}
				return fmt.Sprintf("нет пути в %s", args[0])
			}
		}
		return errorMessage
	},

	// надеть
	"одеть": func(Player *player, args ...string) string {
		if len(args) == 1 {
			msg, err := takeItem(Player, args[0], wearableItem)
			if !err {
				return fmt.Sprintf("вы одели: %s", msg)
			}
			return msg
		}
		return errorMessage
	},

	"взять": func(Player *player, args ...string) string {
		if len(args) == 1 {
			if _, ok := Player.inventory["рюкзак"]; !ok {
				return "некуда класть"
			}
			msg, err := takeItem(Player, args[0], takableItem)
			if !err {
				return fmt.Sprintf("предмет добавлен в инвентарь: %s", msg)
			}
			return msg
		}
		return errorMessage
	},

	"применить": func(Player *player, args ...string) string {
		if len(args) == 2 {
			if currentRoom, okRoom := Player.currentWorld.rooms[Player.currentRoomName]; okRoom {
				if itemInInv, ok := Player.inventory[args[0]]; ok {
					for _, app := range itemInInv.applicable {
						if args[1] == app {
							var obj = currentRoom.objects[app]
							obj.currentState = 1 - obj.currentState
							currentRoom.objects[app] = obj
							return app + " " + obj.states[obj.currentState]
						}
					}
					return "не к чему применить"
				}
				return fmt.Sprintf("нет предмета в инвентаре - %s", args[0])
			}
		}
		return errorMessage
	},

	"debug": func(Player *player, args ...string) string {
		return fmt.Sprint(Player.currentRoomName, Player.inventory)
	},
}
