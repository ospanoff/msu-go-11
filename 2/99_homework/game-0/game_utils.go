package main

import "sort"

func getMapValuesSortedByKey(m map[string]item) []item {
	keys := make([]string, len(m))
	var i int
	for key := range m {
		keys[i] = key
		i++
	}
	sort.Strings(keys)
	values := make([]item, len(m))
	for i, key := range keys {
		values[i] = m[key]
	}
	return values
}
