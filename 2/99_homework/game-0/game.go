package main

import (
	"bufio"
	"fmt"
	"os"
)

var players []player
var currentPlayer = -1

var gameWorld = createWorld()

func initGame() {
	gameWorld.resetWorld()
	players = append(players, player{&gameWorld, "кухня", map[string]item{}})
	currentPlayer++
}

func main() {
	initGame()
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("> ")
	cmd, _ := reader.ReadString('\n')
	for ; len(cmd) > 1; cmd, _ = reader.ReadString('\n') {
		fmt.Println(handleCommand(cmd[:len(cmd)-1]))
		fmt.Print("> ")
	}
	fmt.Println()
}
