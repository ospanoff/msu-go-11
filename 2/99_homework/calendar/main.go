package main

import "time"

// Calendar class
type Calendar struct {
	parsed time.Time
}

// CurrentQuarter method
func (cal Calendar) CurrentQuarter() int {
	m := int(cal.parsed.Month())
	return m/3 + boolToInt(m%3 > 0)
}

// NewCalendar creates Calendar object
func NewCalendar(parsed time.Time) *Calendar {
	return &Calendar{parsed}
}

// boolToInt converts bool to int
func boolToInt(mod bool) int {
	if mod {
		return 1
	}
	return 0
}

func main() {
}
