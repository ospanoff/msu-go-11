package main

import "math"

// Sqrt TODO: Реализовать вычисление Квадратного корня
func Sqrt(x float64) float64 {
	switch {
	case x < 0:
		return math.NaN()
	case x == 0:
		return 0
	}

	var xK = x
	var TOL = 0.0001
	for {
		xK = 0.5 * (xK + x/xK)
		if (xK*xK - x) < TOL {
			return xK
		}
	}
}

func main() {
}
