package main

import "fmt"

type memoizeFunction func(int, ...int) interface{}

var fibonacci memoizeFunction
var romanForDecimal memoizeFunction
var sum memoizeFunction

var i, j int

func memoize(function memoizeFunction) memoizeFunction {
	var memo = map[string]interface{}{}
	return func(n int, args ...int) interface{} {
		var s = fmt.Sprint(n, args)
		if v, ok := memo[s]; ok {
			i++
			return v
		}
		j++
		memo[s] = function(n, args...)

		return memo[s]
	}
}

func init() {
	fibonacci = memoize(func(n int, args ...int) interface{} {
		if n <= 1 {
			return n
		}
		return fibonacci(n-1).(int) + fibonacci(n-2).(int)
	})

	// no effect in memoization
	romanForDecimal = memoize(func(n int, args ...int) interface{} {
		var res string
		var huns = []string{"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}
		var tens = []string{"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}
		var ones = []string{"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}

		//  Add 'M' until we drop below 1000.
		for ; n >= 1000; n -= 1000 {
			res += "M"
		}

		// Add each of the correct elements, adjusting as we go.
		res += huns[n/100]
		n %= 100

		res += tens[n/10]
		n %= 10

		res += ones[n]

		return res
	})

	// sums n times elements from argument
	sum = memoize(func(n int, args ...int) interface{} {
		var innerSum = 0
		for _, arg := range args {
			innerSum += arg
		}
		if n == 0 {
			return 0
		}
		return innerSum + sum(n-1, args...).(int)
	})
}

func main() {
	fmt.Println("Sum_1 =", sum(10, []int{2, 3, 4, 5}...).(int))
	fmt.Println("cached calls:", i, "new calls:", j)

	i, j = 0, 0
	fmt.Println("Sum_2 =", sum(9, []int{2, 3, 4, 5}...).(int))
	fmt.Println("cached calls:", i, "new calls:", j)

	i, j = 0, 0
	fmt.Println("Fibonacci(100) =", fibonacci(100).(int))
	fmt.Println("cached calls:", i, "new calls:", j)

	for _, x := range []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
		14, 15, 16, 17, 18, 19, 20, 25, 30, 40, 50, 60, 69, 70, 80,
		90, 99, 100, 200, 300, 400, 500, 600, 666, 700, 800, 900,
		1000, 1009, 1444, 1666, 1945, 1997, 1999, 2000, 2008, 2010,
		2012, 2500, 3000, 3999} {
		fmt.Printf("%4d = %s\n", x, romanForDecimal(x).(string))
	}
}
