package main

import "reflect"

func getNullType(t string) string {
	switch t {
	case "bool":
		return "Bool"

	case "string":
		return "String"

	case "byte", "int", "int8", "int16", "int32", "int64",
		"uint", "uint8", "unit8", "uint16", "uint32", "uint64":
		return "Int64"

	case "float32", "float64":
		return "Float64"

	default:
		return ""
	}
}

func notLast(x int, a interface{}) bool {
	return x != reflect.ValueOf(a).Len()-1
}
