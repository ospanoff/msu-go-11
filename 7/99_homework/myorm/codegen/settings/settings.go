package settings

import "log"

// ORMName is the name of ORM
const ORMName = "MyORM"

// ORMShort is the short name of the ORM
const ORMShort = "myorm"

// ORMCmt used as an indicator of a struct for code generation
const ORMCmt = ORMShort + ":"

// errPref is a prefix for generator error
const errPref = ORMName + ":"

// CheckErr checks errors
func CheckErr(err error) {
	if err != nil {
		LogErr(err.Error(), true)
	}
}

// LogErr logs and error
func LogErr(msg string, fatal bool) {
	if fatal {
		log.Fatalln(errPref+" FATAL ERROR:", msg)
	}
	log.Println(errPref, msg)
}
