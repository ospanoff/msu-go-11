package test2

//myorm:test2Database
type TEST int

//myorm:test2Database1
type TEST21 struct {
	hidden int64 `myorm:"column:H;null;-"`
	ID     uint  `myorm:"primary_key;column:pk"`
}

//myorm:test2Database2
type TEST22 struct {
	hidden int64 `myorm:"column:H;null"`
	Id     uint  `myorm:"primary_key;column:pk"`
}

//myorm:
type TEST22 struct {
}

//notmyorm:
type TEST22 struct {
}
