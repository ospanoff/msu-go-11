package test1

//myorm:test1Database
type TEST1 struct {
	ID     int    `myorm:"primary_key"`
	null   string `myorm:"null"`
	hidden string `myorm:"-"`
}
