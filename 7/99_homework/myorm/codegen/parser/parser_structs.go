package parser

import (
	"fmt"
	"go/ast"
	"msu-go-11/7/99_homework/myorm/codegen/settings"
	"regexp"
	"strings"
)

type dbField struct {
	Name     string
	Type     string
	Column   string
	Nullable bool
	Pk       bool
}

var reTag = regexp.MustCompile(`"[\w:\-;]+"`)
var supportedTypes = map[string]bool{
	"bool":   true,
	"string": true,
	"int":    true, "int8": true, "int16": true, "int32": true, "int64": true,
	"uint": true, "uint8": true, "unit8": true, "uint16": true, "uint32": true, "uint64": true,
	"byte":    true,
	"float32": true, "float64": true,
}

func (f *dbField) fill(field *ast.Field) bool {
	f.Name = field.Names[0].String()
	f.Type = field.Type.(*ast.Ident).String()
	if _, ok := supportedTypes[f.Type]; !ok {
		settings.LogErr(f.Type+" is not supported", true)
	}
	if field.Tag != nil {
		var tag = field.Tag.Value[1:]
		if strings.HasPrefix(tag, settings.ORMCmt) {
			var param = strings.Trim(reTag.FindString(tag), `"`)
			for _, param := range strings.Split(param, ";") {
				switch param {
				case "primary_key":
					f.Pk = true
				case "null":
					f.Nullable = true
				case "-":
					return false
				default:
					var params = strings.Split(param, ":")
					if len(params) == 2 {
						switch params[0] {
						case "column":
							if params[1] != "" {
								f.Column = params[1]
								continue
							}
						}
					}
					settings.LogErr("Syntax error: "+fset.Position(field.Tag.Pos()).String(), true)
				}
			}
		}
	}
	if f.Column == "" {
		f.Column = strings.ToLower(f.Name)
	}

	return true
}

type ormStruct struct {
	Name         string
	TableName    string
	PkIndx       int
	HasNullField bool
	Fields       []dbField
}

func (os *ormStruct) addField(field *ast.Field) {
	var f = dbField{}
	if f.fill(field) {
		var fName = strings.ToLower(f.Name)
		for _, fld := range os.Fields {
			if fName == fld.Name {
				settings.LogErr(
					fmt.Sprintf("Similar or the same field names in %s struct", os.Name), true,
				)
			}
		}
		if f.Pk {
			if os.PkIndx > 0 {
				settings.LogErr("pk field already exists: "+os.Fields[os.PkIndx].Name, true)
			}
			os.PkIndx = len(os.Fields)
		}
		if f.Nullable {
			os.HasNullField = true
		}
		os.Fields = append(os.Fields, f)
	}
}

// ORMPackage contains of structs with settings.ORMCmt prefix
type ORMPackage struct {
	Name    string
	Structs []ormStruct
}

func (op *ORMPackage) addStruct(name string, tableName string) {
	op.Structs = append(op.Structs, ormStruct{Name: name, TableName: tableName, PkIndx: -1})
}

func (op *ORMPackage) getLastStruct() *ormStruct {
	return &op.Structs[len(op.Structs)-1]
}

func (op *ORMPackage) handleStructs() {
	for _, s := range op.Structs {
		if s.PkIndx == -1 {
			settings.LogErr(
				fmt.Sprintf("Struct %s has no pk", s.Name), true,
			)
		}
		if len(s.Fields) == 1 {
			settings.LogErr(
				fmt.Sprintf("WARNING: Struct %s has only pk", s.Name), false,
			)
		}
	}
}
