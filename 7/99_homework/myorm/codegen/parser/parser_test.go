package parser

import (
	"log"
	"reflect"
	"testing"
)

var logFatalln = log.Fatalln

func TestParse(t *testing.T) {
	for i, test := range []struct {
		fname    string
		expected ORMPackage
	}{
		{"testCases/test1.go", test1},
		{"testCases/test2.go", test2},
	} {
		answer := Parse(test.fname)
		if !reflect.DeepEqual(answer, test.expected) {
			t.Errorf("Case [%d]: failed\n\tEXPECTED\n %v\n\tGOT\n %v", i+1, test.expected, answer)
		}
	}
}

var test1 = ORMPackage{
	Name: "test1",
	Structs: []ormStruct{
		ormStruct{
			Name:         "TEST1",
			TableName:    "test1Database",
			PkIndx:       0,
			HasNullField: true,
			Fields: []dbField{
				dbField{
					Name:   "ID",
					Column: "id",
					Pk:     true,
					Type:   "int",
				},
				dbField{
					Name:     "null",
					Column:   "null",
					Nullable: true,
					Type:     "string",
				},
			},
		},
	},
}

var test2 = ORMPackage{
	Name: "test2",
	Structs: []ormStruct{
		ormStruct{
			Name:         "TEST21",
			TableName:    "test2Database1",
			PkIndx:       0,
			HasNullField: false,
			Fields: []dbField{
				dbField{
					Name:   "ID",
					Column: "pk",
					Pk:     true,
					Type:   "uint",
				},
			},
		},
		ormStruct{
			Name:         "TEST22",
			TableName:    "test2Database2",
			PkIndx:       1,
			HasNullField: true,
			Fields: []dbField{
				dbField{
					Name:     "hidden",
					Column:   "H",
					Nullable: true,
					Type:     "int64",
				},
				dbField{
					Name:   "Id",
					Column: "pk",
					Pk:     true,
					Type:   "uint",
				},
			},
		},
	},
}
