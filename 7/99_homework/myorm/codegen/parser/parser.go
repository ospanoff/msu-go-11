package parser

import (
	"go/ast"
	"go/parser"
	"go/token"
	"msu-go-11/7/99_homework/myorm/codegen/settings"
	"strings"
)

var fset *token.FileSet

// Parse parses file to ormPkg
func Parse(pathToFile string) ORMPackage {
	fset = token.NewFileSet()
	file, err := parser.ParseFile(fset, pathToFile, nil, parser.ParseComments)
	settings.CheckErr(err)

	var ormPkg = ORMPackage{}
	var lastTypeSpecName string
	var lastTableName string

	ast.Inspect(file, func(n ast.Node) bool {
		switch n := n.(type) {
		case *ast.File:
			ormPkg.Name = n.Name.String()

		case *ast.GenDecl:
			var doc = n.Doc.Text()
			if strings.HasPrefix(doc, settings.ORMCmt) {
				var tableName = strings.Trim(strings.Split(doc, " ")[0], "\n")
				tableName = strings.Split(tableName, ":")[1]
				if tableName == "" {
					settings.LogErr("Empty table name: "+fset.Position(n.Doc.Pos()).String(), false)
					settings.LogErr("Skipping...", false)
					return false
				}
				lastTableName = tableName
				return true
			}
			return false

		// Enters here if only we met a doc with ormName prefix
		case *ast.TypeSpec:
			lastTypeSpecName = n.Name.String()

		// Enters here if the struct have a doc with ormName prefix
		case *ast.StructType:
			ormPkg.addStruct(lastTypeSpecName, lastTableName)

		// Enters here if prev node is a struct
		case *ast.Field:
			ormPkg.getLastStruct().addField(n)

		case *ast.FieldList:
		default:
			return false
		}
		return true
	})

	ormPkg.handleStructs()

	return ormPkg
}
