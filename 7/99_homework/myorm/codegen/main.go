package main

import (
	"fmt"
	"html/template"
	"msu-go-11/7/99_homework/myorm/codegen/parser"
	"msu-go-11/7/99_homework/myorm/codegen/settings"
	"os"
	"path"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: ./main path_to_file")
		os.Exit(1)
	}
	var pathToFile = os.Args[1]

	var ormPkg = parser.Parse(pathToFile)

	var tmplDir = "templates/"

	// Generate code
	var tmpl, errT = template.New("").Funcs(template.FuncMap{
		"notlast":     notLast,
		"getNullType": getNullType,
	}).ParseFiles(
		tmplDir+"header.ormt",
		tmplDir+"base.ormt",
	)
	settings.CheckErr(errT)

	for _, Struct := range ormPkg.Structs {
		output, err := os.Create(path.Dir(pathToFile) + "/" + Struct.TableName + "_" + settings.ORMShort + ".go")
		settings.CheckErr(err)

		errT = tmpl.ExecuteTemplate(output, "base", map[string]interface{}{
			"ormName":     settings.ORMName,
			"packageName": ormPkg.Name,
			"struct":      Struct,
		})
		settings.CheckErr(errT)

		output.Close()
	}

	if len(ormPkg.Structs) > 0 {
		// Generate utils file
		tmpl, errT = template.New("utils.ormt").ParseFiles(tmplDir + "utils.ormt")
		settings.CheckErr(errT)

		output, err := os.Create(path.Dir(pathToFile) + "/" + "utils" + "_" + settings.ORMShort + ".go")
		settings.CheckErr(err)
		defer output.Close()

		errT = tmpl.Execute(output, map[string]interface{}{
			"packageName": ormPkg.Name,
		})
		settings.CheckErr(errT)
	} else {
		settings.LogErr("No structures found", false)
	}
}
