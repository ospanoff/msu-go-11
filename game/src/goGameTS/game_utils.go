package main

import (
	"log"
	"math/rand"
	"sort"
)

func getMapValuesSortedByKey(m map[string]Item) []Item {
	keys := make([]string, len(m))
	var i int
	for key := range m {
		keys[i] = key
		i++
	}
	sort.Strings(keys)
	values := make([]Item, len(m))
	for i, key := range keys {
		values[i] = m[key]
	}
	return values
}

func checkErr(err error) {
	if err != nil {
		log.Fatalln("FATAL ERROR:", err)
	}
}

var letterRunes = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
