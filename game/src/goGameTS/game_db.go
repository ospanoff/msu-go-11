package main

import (
	"database/sql"
	"encoding/json"
	"github.com/lib/pq"
	"os"
	"time"
)

type commands struct {
	id      int
	from    string
	Command string
	Result  map[string]string
	Time    int64
}
type user struct {
	name      string
	userAgent string
}

var userDB = map[string]string{
	"ospanoff": "12345",
}
var sessions = map[string]user{}

var db *sql.DB

const (
	dbUSER = "postgres"
	dbNAME = "testdb"
)

func initDB() {
	var err error
	// dbinfo := fmt.Sprintf("user=%s dbname=%s sslmode=disable", dbUSER, dbNAME)
	var dbinfo, _ = pq.ParseURL(os.Getenv("DATABASE_URL"))
	db, err = sql.Open("postgres", dbinfo)
	checkErr(err)
	checkErr(db.Ping())
}

func addToDB(from, command, result string) int {
	var lastInsertID int
	var err = db.QueryRow(
		`INSERT INTO commands ("from", command, result, time)
	    VALUES ($1,$2,$3,$4)
        RETURNING id;`,
		from,
		command,
		result,
		time.Now().Unix(),
	).Scan(&lastInsertID)
	checkErr(err)
	return lastInsertID
}

func getFromDB(from string) []commands {
	var res = []commands{}
	var result string

	var rows, err = db.Query(
		`SELECT command, result, time
        FROM commands
        WHERE "from"=$1
        ORDER BY id DESC
        LIMIT 100`, from)
	checkErr(err)
	defer rows.Close()
	for rows.Next() {
		var cmd = commands{}
		err = rows.Scan(&cmd.Command, &result, &cmd.Time)
		json.Unmarshal([]byte(result), &cmd.Result)
		checkErr(err)
		res = append(res, cmd)
	}
	checkErr(rows.Err())

	return res
}

func removeFromDB(from string) {
	var _, err = db.Query(`DELETE FROM commands WHERE "from"=$1`, from)
	checkErr(err)
}
