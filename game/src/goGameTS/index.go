package main

import (
	"fmt"
	"net/http"
	"text/template"
	"time"
)

const (
	pathToTemplates = "src/goGameTS/templates/"
	// pathToTemplates = "templates/"
)

func handleIndexPage() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "POST" {
			checkErr(r.ParseForm())
			var username = r.Form["login"][0]
			var password = r.Form["password"][0]
			var userAgent = r.UserAgent()
			if pass, ok := userDB[username]; ok && password == pass {
				var sessionID = randStringRunes(25)
				var expiration = time.Now().Add(time.Hour)
				sessions[sessionID] = user{username, userAgent}
				cookie := http.Cookie{
					Name:    "session_id",
					Value:   sessionID,
					Expires: expiration,
				}
				http.SetCookie(w, &cookie)
				http.Redirect(w, r, "/", http.StatusFound)
			} else {
				fmt.Fprint(w, "Неверный логин/пароль")
			}
		} else {
			sessID, err := r.Cookie("session_id")
			var sessionID string
			if err == nil {
				sessionID = sessID.Value
			}
			var session, sessExist = sessions[sessionID]
			if err == http.ErrNoCookie || !sessExist {
				var tmpl, errT = template.New("login.html").ParseFiles(pathToTemplates + "login.html")
				checkErr(errT)
				errT = tmpl.Execute(w, "")
				checkErr(errT)
				return
			} else if err != nil {
				checkErr(err)
			}
			var tmpl = template.New("index.html")
			tmpl = tmpl.Funcs(template.FuncMap{
				"getDate":     getDate,
				"resToString": resToString,
			})
			tmpl, errT := tmpl.ParseFiles(pathToTemplates + "index.html")
			checkErr(errT)
			errT = tmpl.Execute(w, map[string]interface{}{
				"user_name": session.name,
				"user_ua":   session.userAgent,
				"cmds":      getFromDB(session.name),
			})
			checkErr(errT)
		}
	})
}

func getDate(ts int64) string {
	t := time.Unix(ts, 0)
	return t.Format("02.01.2006 15:04:05")
}

func resToString(res map[string]string) string {
	var s string
	for uname, result := range res {
		s = uname + ": " + result
	}
	return s
}
