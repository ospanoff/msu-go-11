package main

// TODO: Make common data architect

import (
	"encoding/json"
	// "github.com/go-telegram-bot-api/telegram-bot-api" // for local testing
	"gopkg.in/telegram-bot-api.v4"
	"log"
	"net/http"
	"os"
)

var gameWorld World

const (
	// WebhookURL При старте приложения, оно скажет телеграму ходить с обновлениями по этому URL
	WebhookURL = "https://go-game-ts.herokuapp.com/bot"
	// AccessToken for TG Bot
	AccessToken = "329968365:AAGJOcD8Wuto8vJc470d6KBC5iKugnLvADw"
)

func main() {
	initDB()
	// Heroku прокидывает порт для приложения в переменную окружения PORT
	port := os.Getenv("PORT")
	bot, err := tgbotapi.NewBotAPI(AccessToken)
	if err != nil {
		log.Fatal(err)
	}
	bot.Debug = true
	log.Printf("Authorized on account %s", bot.Self.UserName)

	_, err = bot.SetWebhook(tgbotapi.NewWebhook(WebhookURL))
	if err != nil {
		log.Fatal(err)
	}

	updates := bot.ListenForWebhook("/bot")
	handleIndexPage()
	go http.ListenAndServe(":"+port, nil)

	// Init the game
	initGame()

	for update := range updates {
		var messageText string
		log.Println("received text: ", update.Message.Text)

		var playerName = update.Message.From.String()
		var playerID = update.Message.From.ID
		var chatID = update.Message.Chat.ID

		switch update.Message.Text {
		case `/start`:
			if _, ok := gameWorld.players[playerID]; !ok {
				var player = newPlayer(playerID, playerName)
				addPlayer(player)
				if _, ok := userDB[playerName]; !ok {
					userDB[playerName] = randStringRunes(5)
				}
				go func(player *Player, chatID int64) {
					var output = player.GetOutput()
					for {
						select {
						case msg, ok := <-output:
							if !ok { // chan is closed, if the player has left
								return
							}
							if player.lastAnsTo == "" {
								bot.Send(tgbotapi.NewMessage(chatID, msg))
							}
							if player.lastAnsTo == "" {
								player.lastAnsTo = "Себе"
							}
							var resultStr, _ = json.Marshal(map[string]string{player.lastAnsTo: msg})
							addToDB(player.name, player.lastCMD, string(resultStr))
							player.lastAnsTo = ""
							player.lastCMD = ""
						case <-player.timer.C:
							bot.Send(tgbotapi.NewMessage(chatID, `Вы были кикнуты за простой`))
							kickPlayer(player)
							return
						case <-player.resetTimer:
							player.timer.Reset(gameWorld.afkTime)
						}
					}
				}(player, chatID)

				messageText = `Вы начали игру`
			} else {
				messageText = `Вы уже играете`
			}

		case `/stop`:
			if player, ok := gameWorld.players[playerID]; ok {
				kickPlayer(player)
				messageText = `Вы закончили игру`
			} else {
				messageText = `Вы не начали игру`
			}

		case `/reset`:
			if _, ok := gameWorld.admins[playerID]; ok {
				for _, playerPtr := range gameWorld.players {
					if !playerPtr.isAdmin {
						bot.Send(tgbotapi.NewMessage(int64(playerPtr.id), `Игра была перезапущена`))
					}
					// kickPlayer(playerPtr)
				}
				for adminID := range gameWorld.admins {
					bot.Send(tgbotapi.NewMessage(int64(adminID), `Игра была перезапущена`))
				}
				initGame() // this will kick all players
				messageText = ``
			} else {
				messageText = errorMessage
			}

		case `/password`:
			if player, ok := gameWorld.players[playerID]; ok {
				messageText = userDB[player.name]
			} else {
				messageText = `Вы не начали игру`
			}

		case `/clean_history`:
			if player, ok := gameWorld.players[playerID]; ok {
				removeFromDB(player.name)
				messageText = `База очищена`
			} else {
				messageText = `Вы не начали игру`
			}

		case `/debug`:
			if _, ok := gameWorld.admins[playerID]; ok {
				messageText = `debug`
			} else {
				messageText = errorMessage
			}

		default:
			if player, ok := gameWorld.players[playerID]; ok {
				player.HandleInput(update.Message.Text)
				player.resetTimer <- true
				messageText = ``
			} else {
				messageText = `Вы не начали игру`
			}
		}

		if messageText != `` {
			bot.Send(tgbotapi.NewMessage(chatID, messageText))
		}
	}
}
