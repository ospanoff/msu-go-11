package main

import "time"

func getItem(name string, count int, room string) Item {
	switch name {
	case "рюкзак":
		return Item{wearableItem, "рюкзак", count, []string{}, room}
	case "ключи":
		return Item{takableItem, "ключи", count, []string{"дверь"}, room}
	case "конспекты":
		return Item{takableItem, "конспекты", count, []string{}, room}
	default:
		return Item{}
	}
}

func createRooms() map[string]Room {
	return map[string]Room{
		"кухня": Room{
			name:      "кухня",
			items:     map[string]Item{},
			nextRooms: []string{"коридор"},
			obstacles: []string{""},
			message:   "кухня, ничего интересного",
			objects:   map[string]Object{},
			players:   map[int]*Player{},
		},
		"коридор": Room{
			name:      "коридор",
			items:     map[string]Item{},
			nextRooms: []string{"кухня", "комната", "улица"},
			obstacles: []string{"", "", "дверь"},
			message:   "ничего интересного",
			objects: map[string]Object{
				"дверь": Object{1, []string{"открыта", "закрыта"}}, // 1 if bloked else 0
			},
			players: map[int]*Player{},
		},
		"комната": Room{
			name: "комната",
			items: map[string]Item{
				"рюкзак":    getItem("рюкзак", 1, "комната"),
				"ключи":     getItem("ключи", 1, "комната"),
				"конспекты": getItem("конспекты", 1, "комната"),
			},
			nextRooms: []string{"коридор"},
			obstacles: []string{""},
			message:   "ты в своей комнате",
			objects:   map[string]Object{},
			players:   map[int]*Player{},
		},
		"улица": Room{
			name:      "улица",
			items:     map[string]Item{},
			nextRooms: []string{"домой"},
			obstacles: []string{""},
			message:   "на улице весна",
			objects:   map[string]Object{},
			players:   map[int]*Player{},
		},
	}
}

func createWorld() World {
	return World{
		rooms:   createRooms(),
		players: map[int]*Player{},
		admins: map[int]bool{
			61146143: true,
		},
		afkTime: 15 * time.Minute,
	}
}

func initGame() {
	gameWorld = createWorld()
}

// newPlayer creates new player
func newPlayer(id int, name string) *Player {
	return &Player{
		id:              id,
		name:            name,
		isAdmin:         gameWorld.admins[id],
		currentWorld:    &gameWorld,
		currentRoomName: "кухня",
		inventory:       map[string]Item{},
		outputChan:      make(chan string),
		timer:           time.NewTimer(gameWorld.afkTime),
		resetTimer:      make(chan bool),
	}
}

func addPlayer(player *Player) {
	player.currentRoomName = "кухня"
	gameWorld.rooms[player.currentRoomName].players[player.id] = player
	gameWorld.players[player.id] = player
}

func kickPlayer(player *Player) {
	for itemName, item := range player.inventory {
		var roomItem = gameWorld.rooms[item.roomName].items[itemName]
		roomItem.quantity += item.quantity
		gameWorld.rooms[item.roomName].items[itemName] = item
	}
	close(player.outputChan)
	close(player.resetTimer)
	delete(gameWorld.rooms[player.currentRoomName].players, player.id)
	delete(gameWorld.players, player.id)
}
