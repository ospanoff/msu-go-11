package main

import (
	"fmt"
	"strings"
	"time"
)

// ###############
// WORLD impl.
// ###############

// World class
type World struct {
	rooms   map[string]Room
	players map[int]*Player
	admins  map[int]bool
	afkTime time.Duration
}

// ###############
// PLAYER impl.
// ###############

// Player class
type Player struct {
	id              int
	name            string
	isAdmin         bool
	currentWorld    *World
	currentRoomName string
	inventory       map[string]Item
	outputChan      chan string
	timer           *time.Timer
	resetTimer      chan bool
	lastCMD         string
	lastAnsTo       string
}

// GetOutput return output channel
func (p *Player) GetOutput() <-chan string {
	return p.outputChan
}

// HandleInput acts with the player
func (p *Player) HandleInput(s string) {
	p.lastCMD = s
	var args = strings.Split(s, " ")
	var params = args[1:]
	var cmd = args[0]
	var answer = errorMessage
	action, ok := actions[cmd]
	if ok {
		answer = action(p, params...)
	}
	if answer != "" {
		p.outputChan <- answer
	}
}

// ###############
// ROOM impl.
// ###############

// Room class
type Room struct {
	name      string
	items     map[string]Item
	nextRooms []string
	obstacles []string
	message   string
	objects   map[string]Object
	players   map[int]*Player
}

// ###############
// ITEM impl.
// ###############

// Item class: objects that are takable by Player, e.g. key
type Item struct {
	itemType   string
	name       string
	quantity   int
	applicable []string
	roomName   string
}

const (
	wearableItem = "wearable"
	takableItem  = "takable"
)

// ###############
// OBJECT impl.
// ###############

// Object class: objects that are built in rooms, e.g. door
type Object struct {
	currentState int
	states       []string
}

// ###############
// GAME FUNСTIONS
// ###############
var errorMessage = "неизвестная команда"

func takeItem(player *Player, itemName string, itemType string) (string, bool) {
	if currentRoom, okRoom := player.currentWorld.rooms[player.currentRoomName]; okRoom {
		if itemInRoom, okItem := currentRoom.items[itemName]; okItem {
			if itemInRoom.itemType != itemType {
				return errorMessage, true
			}
			if itemInRoom.quantity > 0 {
				itemInRoom.quantity--
				currentRoom.items[itemName] = itemInRoom

				if it, okIt := player.inventory[itemName]; okIt {
					it.quantity++
					player.inventory[itemName] = it
				} else {
					itemInRoom.quantity = 1
					player.inventory[itemName] = itemInRoom
				}
				return itemName, false
			}
		}
		return "нет такого", true
	}
	return errorMessage, true
}

var actions = map[string]func(*Player, ...string) string{
	"осмотреться": func(player *Player, args ...string) string {
		if len(args) == 0 {
			var answer string
			// FIXME DELETE THIS WORKAROUND
			if player.currentRoomName == "кухня" {
				var hasBackpack = false
				for name := range player.inventory {
					if name == "рюкзак" {
						hasBackpack = true
						break
					}
				}
				if hasBackpack {
					answer = "ты находишься на кухне, на столе чай, надо идти в универ. можно пройти - коридор"
				} else {
					answer = "ты находишься на кухне, на столе чай, надо собрать рюкзак и идти в универ. можно пройти - коридор"
				}
			} else {
				var w, t int
				if currentRoom, ok := player.currentWorld.rooms[player.currentRoomName]; ok {
					var wearableItems = "на стуле - "
					var takableItems = "на столе: "
					var allItems = getMapValuesSortedByKey(currentRoom.items)
					for _, item := range allItems {
						if item.quantity > 0 {
							if item.itemType == wearableItem {
								wearableItems += item.name + ", "
								w++
							} else if item.itemType == "takable" {
								takableItems += item.name + ", "
								t++
							}
						}
					}
					if w == 0 {
						wearableItems = ""
						if t != 0 {
							takableItems = takableItems[:len(takableItems)-2]
						}
					} else {
						wearableItems = wearableItems[:len(wearableItems)-2]
					}
					if t == 0 {
						takableItems = ""
					}
					if w+t == 0 {
						takableItems = "пустая комната"
					}
					answer = fmt.Sprintf("%s%s. можно пройти - %s", takableItems, wearableItems, strings.Join(currentRoom.nextRooms, ", "))
				}
			}
			var neighbours = ". Кроме вас тут ещё "
			for _, playerPtr := range player.currentWorld.rooms[player.currentRoomName].players {
				if playerPtr != player {
					neighbours += playerPtr.name + ", "
				}
			}
			if neighbours == ". Кроме вас тут ещё " {
				neighbours = ".."
			}
			return answer + neighbours[:len(neighbours)-2]
		}
		return errorMessage
	},

	"идти": func(player *Player, args ...string) string {
		if len(args) == 1 {
			if currentRoom, ok := player.currentWorld.rooms[player.currentRoomName]; ok {
				for i, r := range currentRoom.nextRooms {
					if r == args[0] {
						var obstacle = currentRoom.obstacles[i]
						if obj, okObj := currentRoom.objects[obstacle]; okObj {
							if obj.currentState == 1 {
								return obstacle + " " + obj.states[obj.currentState]
							}
						}
						if newRoom, ok := player.currentWorld.rooms[args[0]]; ok {
							player.currentRoomName = args[0]
							delete(currentRoom.players, player.id)
							newRoom.players[player.id] = player
							return fmt.Sprintf("%s. можно пройти - %s", newRoom.message, strings.Join(newRoom.nextRooms, ", "))
						}
						return errorMessage
					}
				}
				return fmt.Sprintf("нет пути в %s", args[0])
			}
		}
		return errorMessage
	},

	// надеть
	"надеть": func(player *Player, args ...string) string {
		if len(args) == 1 {
			msg, err := takeItem(player, args[0], wearableItem)
			if !err {
				return fmt.Sprintf("вы надели: %s", msg)
			}
			return msg
		}
		return errorMessage
	},

	"взять": func(player *Player, args ...string) string {
		if len(args) == 1 {
			if _, ok := player.inventory["рюкзак"]; !ok {
				return "некуда класть"
			}
			msg, err := takeItem(player, args[0], takableItem)
			if !err {
				return fmt.Sprintf("предмет добавлен в инвентарь: %s", msg)
			}
			return msg
		}
		return errorMessage
	},

	"применить": func(player *Player, args ...string) string {
		if len(args) == 2 {
			if currentRoom, okRoom := player.currentWorld.rooms[player.currentRoomName]; okRoom {
				if itemInInv, ok := player.inventory[args[0]]; ok {
					for _, app := range itemInInv.applicable {
						if args[1] == app {
							var obj = currentRoom.objects[app]
							obj.currentState = 1 - obj.currentState
							currentRoom.objects[app] = obj
							return app + " " + obj.states[obj.currentState]
						}
					}
					return "не к чему применить"
				}
				return fmt.Sprintf("нет предмета в инвентаре - %s", args[0])
			}
		}
		return errorMessage
	},

	"сказать": func(player *Player, args ...string) string {
		var message = player.name + " говорит: " + strings.Join(args, " ")
		for _, playerPtr := range player.currentWorld.rooms[player.currentRoomName].players {
			playerPtr.outputChan <- message
		}
		player.lastAnsTo = "Всем"
		return message
	},

	"сказать_игроку": func(player *Player, args ...string) string {
		if len(args) > 0 {
			var name = args[0]
			var message = strings.Join(args[1:], " ")
			if len(message) == 0 {
				message = player.name + " выразительно молчит, смотря на вас"
			} else {
				message = player.name + " говорит вам: " + message
			}
			for _, playerPtr := range player.currentWorld.rooms[player.currentRoomName].players {
				if playerPtr.name == name {
					playerPtr.outputChan <- message
					player.lastAnsTo = name
					return message
				}
			}
			return "тут нет такого игрока" // "нету игрока " + name + " в данной комнате"
		}
		return "напишите имя получателя"
	},
}
