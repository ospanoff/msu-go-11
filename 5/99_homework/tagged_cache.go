package main

import (
	"encoding/json"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"reflect"
)

var (
	c redis.Conn
)

type funcToRebuild func() (interface{}, []string)
type cacheItem struct {
	Data interface{}
	Tags map[string]int
}

// TcacheGet ...
func TcacheGet(mkey string, buildCache funcToRebuild) (interface{}, error) {
	var item, err = redis.String(c.Do("GET", mkey))
	if err == redis.ErrNil {
		return createRec(mkey, buildCache)
	} else if err != nil {
		panicOnErr(err)
	}
	cItem := cacheItem{}
	_ = json.Unmarshal([]byte(item), &cItem)

	if checkKeys(cItem.Tags) {
		return cItem.Data, nil
	}
	return createRec(mkey, buildCache)
}

func main() {
	var err error
	c, err = redis.DialURL("redis://user:@localhost:6379/0")
	panicOnErr(err)
	defer c.Close()

	var bC = func() (interface{}, []string) {
		fmt.Println("I've been called")
		return "data", []string{"1", "2"}
	}

	fmt.Println(TcacheGet("key1", bC))
	fmt.Println(TcacheGet("key1", bC))
	c.Do("INCR", "1")
	fmt.Println(TcacheGet("key1", bC))
}

func checkKeys(tags map[string]int) bool {
	keys := make([]interface{}, 0)
	toCompare := make([]int, 0)
	for key, val := range tags {
		keys = append(keys, key)
		toCompare = append(toCompare, val)
	}

	reply, err := redis.Ints(c.Do("MGET", keys...))
	panicOnErr(err)
	return reflect.DeepEqual(toCompare, reply)
}

func createRec(mkey string, buildCache funcToRebuild) (interface{}, error) {
	var item, tags = buildCache()
	var cItem = cacheItem{
		Data: item,
		Tags: map[string]int{},
	}
	for _, tag := range tags {
		var v, e = redis.Int(c.Do("GET", tag))
		if e == redis.ErrNil {
			c.Do("SET", tag, 1)
			v = 1
		} else if e != nil {
			panicOnErr(e)
		}
		cItem.Tags[tag] = v
	}
	jsonData, _ := json.Marshal(cItem)
	c.Do("SET", mkey, jsonData)
	return item, nil
}

func panicOnErr(err error) {
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
}
